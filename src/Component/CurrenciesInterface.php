<?php

declare(strict_types=1);

namespace SubbkovOpenSource\ListCurrencies\Component;

interface CurrenciesInterface
{
    /**
     * @param array $arrayRequest
     *
     * @throws CurrenciesException
     *
     * @return array
     */
    public function getData(array $arrayRequest): array;
}
