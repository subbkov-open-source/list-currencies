<?php

declare(strict_types=1);

namespace SubbkovOpenSource\ListCurrencies\Component;

use Symfony\Component\Yaml\Yaml;

class Currencies implements CurrenciesInterface
{
    /**
     * @var array
     *
     * Array of currencies from Yaml file
     */
    private $currenciesYaml = [];

    /**
     * @var array
     *
     * Array of names and currency symbols
     *           [name] = symbol
     */
    private $currenciesList = [];

    /**
     * @var array
     *
     * Numbered list of all currencies
     *           [id] = [id => ..., name => ..., symbol => ...]
     */
    private $currenciesNumbered = [];

    /**
     * @var array
     *
     * Array of currencies by queue
     *           [queue][] = [name => ..., symbol => ...]
     */
    private $currenciesByQueue = [];

    /**
     * @var \ArrayObject
     *
     * Currencies relations list
     *           [1_2] = 1,
     *           [2_1] = 2,
     *           ...
     */
    private $currenciesRelationsIterator;

    /**
     * @var array
     *
     * Array Request
     */
    private $request = [];

    /**
     * @var array
     *
     * List of unique currencies from the request
     *           [name|symbol] = ...
     */
    private $uniqueListRequest = [];

    /**
     * @var array
     *
     * List of currency relations from the request
     *           [name|symbol.'_'.name|symbol] = ...
     */
    private $relationsListRequestByName = [];

    /**
     * @var array
     *
     * Prepare array response currencies numbered
     *           [id] = [name => ..., symbol => ...]
     */
    private $responseCurrenciesNumbered = [];

    /**
     * {@inheritdoc}
     */
    public function getData(array $arrayRequest): array
    {
        $this
            ->processingCurrenciesYaml(Yaml::parseFile(__DIR__ . '/../Data/list-currencies.yaml'))
            ->prepareRequest($arrayRequest);

        return $this->init();
    }

    //region Preparation of primary data

    /**
     * Processing of a common array of currencies.
     *
     * @param array $currenciesYaml
     *
     * @throws CurrenciesException
     *
     * @return $this
     */
    private function processingCurrenciesYaml(array $currenciesYaml): self
    {
        if (0 === \count($currenciesYaml)) {
            throw new CurrenciesException('Currency list file is empty');
        }
        $this->currenciesYaml = $currenciesYaml;
        $this
            ->prepareListCurrencies()
            ->prepareCurrenciesByNumberedAndByQueue();

        return $this;
    }

    /**
     * Preparation of the Array of Currency Names and Symbols
     *           [name] = symbol.
     *
     * @return $this
     */
    private function prepareListCurrencies(): self
    {
        foreach ($this->currenciesYaml as $typeCurrency => $list) {
            $this->currenciesList = \array_merge(
                $this->currenciesList,
                \array_column($list, 'symbol', 'name')
            );
        }

        return $this;
    }

    /**
     * Get a pre-prepared array of currencies by queue
     *           [queue][] = [name => ..., symbol => ...].
     *
     * @return array
     */
    private function getPrePrepareCurrenciesByQueue(): array
    {
        /** @var array $currenciesByDate */
        $currenciesByDate = [];

        foreach ($this->currenciesYaml as $arrayCurrencies) {
            foreach ($arrayCurrencies as $dataCurrency) {
                $currenciesByDate[$dataCurrency['queue']][] = [
                    'symbol' => $dataCurrency['symbol'],
                    'name'   => $dataCurrency['name'],
                ];
            }
        }

        \ksort($currenciesByDate);

        return $currenciesByDate;
    }

    /**
     * Prepare numbered array (currenciesNumbered) and array by queues (currenciesByQueue).
     *
     * @return $this
     */
    private function prepareCurrenciesByNumberedAndByQueue(): self
    {
        /** @var int $i */
        $idCurrency = 0;

        /** @var array $currenciesByDate */
        $currenciesByDate = $this->getPrePrepareCurrenciesByQueue();

        foreach ($currenciesByDate as $queue => $arrayCurrencies) {
            foreach ($arrayCurrencies as $dataCurrency) {
                $this->currenciesByQueue[$queue][++$idCurrency] = $dataCurrency;
                $this->currenciesNumbered[$idCurrency]          = $dataCurrency;
            }
        }

        return $this;
    }

    //endregion

    //region Validation and prepare date from the request

    /**
     * Prepare array Request.
     *
     * @param array $arrayRequest
     *
     * @throws CurrenciesException
     *
     * @return $this
     */
    private function prepareRequest(array $arrayRequest): self
    {
        if (0 === \count($arrayRequest)) {
            throw new CurrenciesException('The array you send is empty');
        }
        $this->request                     = $arrayRequest;
        $this->currenciesRelationsIterator = new \ArrayObject([]);

        $this->prepareUniqueListRequest();

        $this->validationExistenceListRequest()->validationDuplicateListRequest();

        $this
            ->prepareResponseCurrenciesNumbered()
            ->processingKeyRequestRelationsByName($this->relationsListRequestByName)
            ->processingValueRequestRelationsByName($this->relationsListRequestByName);

        return $this;
    }

    /**
     * Prepare list of unique currencies from the request
     *           [name|symbol] = ...
     *
     * @return $this
     */
    private function prepareUniqueListRequest(): self
    {
        /*
         * Get the names and symbols of the request keys
         *           [name|symbol] = ...
         */
        $this->uniqueListRequest = \array_flip(\array_keys($this->request));

        foreach ($this->request as $currencies) {
            /*
             * if the currency has relations
             *           [name|symbol]:
             *              - [name|symbol]
             *              - [name|symbol]
             *              - ...
             */
            if (0 !== \count($currencies)) {
                $this->uniqueListRequest = \array_merge($this->uniqueListRequest, \array_flip($currencies));
            }
        }

        /** @var array|false $uniqueListRequest */
        $uniqueListRequest = \array_combine(
            \array_keys($this->uniqueListRequest),
            \range(1, \count($this->uniqueListRequest))
        );

        if (false === $uniqueListRequest) {
            throw new CurrenciesException('The number of elements in the arrays does not match.');
        }

        // Change array keys
        $this->uniqueListRequest = $uniqueListRequest;

        return $this;
    }

    /**
     * Validation for the existence of currencies sent in the request.
     *
     * @throws CurrenciesException
     *
     * @return $this
     */
    private function validationExistenceListRequest(): self
    {
        /** @var array $errors */
        $errors = [];

        foreach ($this->uniqueListRequest as $currency => $numberCurrency) {
            if (!(isset($this->currenciesList[$currency]) || \in_array($currency, $this->currenciesList, true))) {
                $errors[] = $currency;
            }
        }

        if (0 !== \count($errors)) {
            throw new CurrenciesException('You specified non-existent currencies: ' . \implode(', ', $errors));
        }

        return $this;
    }

    /**
     * Checking the duplicate characters of the currencies specified in the request.
     *
     * @throws CurrenciesException
     *
     * @return $this
     */
    private function validationDuplicateListRequest(): self
    {
        /** @var array $errors */
        $errors = [];

        /**
         * @var array
         *
         * Counting the number of duplicate currency symbols
         */
        $duplicateSymbolCurrenciesList = \array_filter(
            \array_count_values($this->currenciesList),
            function ($count) {
                return $count > 1;
            }
        );

        if (0 !== \count($duplicateSymbolCurrenciesList)) {
            foreach ($this->uniqueListRequest as $currency => $numberCurrency) {
                if (isset($duplicateSymbolCurrenciesList[$currency])) {
                    $errors[] = $currency . ' -> ' . \array_search($currency, $this->currenciesList, true);
                }
            }
        }

        if (0 !== \count($errors)) {
            throw new CurrenciesException('For selected currencies You need to replace symbols with names: '
                . \implode(', ', $errors));
        }

        return $this;
    }

    /**
     * List of currency relations from the request - By key
     *           [name|symbol.'_'.name|symbol] = ...
     *
     * @param array $namesRelationsList
     *
     * @return $this
     */
    private function processingKeyRequestRelationsByName(array &$namesRelationsList): self
    {
        /** @var int $dummyNumber */
        $dummyNumber = \count($namesRelationsList);
        // Key currencies processing
        foreach ($this->request as $currency => $unneededData) {
            foreach ($this->request as $currency2 => $unneededData2) {
                if ($currency !== $currency2) {
                    if (!isset($namesRelationsList[$currency . '_' . $currency2])) {
                        $namesRelationsList[$currency . '_' . $currency2] = ++$dummyNumber;
                    }
                    if (!isset($namesRelationsList[$currency2 . '_' . $currency])) {
                        $namesRelationsList[$currency2 . '_' . $currency] = ++$dummyNumber;
                    }
                }
            }
        }

        return $this;
    }

    /**
     * List of currency relations from the request - By value
     *           [name|symbol.'_'.name|symbol] = ...
     *
     * @param array $namesRelationsList
     *
     * @return $this
     */
    private function processingValueRequestRelationsByName(array &$namesRelationsList): self
    {
        /** @var int $dummyNumber */
        $dummyNumber = \count($namesRelationsList);
        // Value currencies processing
        foreach ($this->request as $currency => $currencyList) {
            foreach ($currencyList as $currency2) {
                if ($currency !== $currency2) {
                    if (!isset($namesRelationsList[$currency . '_' . $currency2])) {
                        $namesRelationsList[$currency . '_' . $currency2] = ++$dummyNumber;
                    }
                    if (!isset($namesRelationsList[$currency2 . '_' . $currency])) {
                        $namesRelationsList[$currency2 . '_' . $currency] = ++$dummyNumber;
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Prepare array response currencies numbered
     *           [id] = [name => ..., symbol => ...].
     *
     * @return $this
     */
    private function prepareResponseCurrenciesNumbered(): self
    {
        $uniqueListRequest                = $this->uniqueListRequest;
        $this->responseCurrenciesNumbered = \array_filter(
            $this->currenciesNumbered,
            function ($dateCurrency) use ($uniqueListRequest) {
                return $uniqueListRequest[$dateCurrency['symbol']] ?? $uniqueListRequest[$dateCurrency['name']] ?? false;
            }
        );

        foreach ($this->responseCurrenciesNumbered as $id => &$date) {
            $date['id'] = $id;
        }

        return $this;
    }

    //endregion

    //region Preparation data for the response

    private function init(): array
    {
        $this->prepareCurrenciesRelationsList();

        return [
            'currencies' => $this->responseCurrenciesNumbered,
            'relations'  => $this->getResponseRelationsCurrencies(),
        ];
    }

    /**
     * Prepare a list of currency relations.
     *
     * @return $this
     */
    private function prepareCurrenciesRelationsList()
    {
        /** @var int $numberRelations */
        $numberRelations = 0;
        /** @var array $currenciesAllList */
        $currenciesAllList = [];
        /** @var int $iMaxIdCurrency */
        $iMaxIdCurrency = \max(\array_keys($this->responseCurrenciesNumbered));
        /** @var bool $isBreak */
        $isBreak = false;
        /** @var array $currenciesList */
        foreach ($this->currenciesByQueue as $currenciesList) {
            $currenciesAllList += $currenciesList;

            foreach ($currenciesAllList as $keyRelations1 => $tmV) {
                foreach ($currenciesAllList as $keyRelations2 => $tmV2) {
                    if ($keyRelations1 !== $keyRelations2) {
                        if (!$this->currenciesRelationsIterator->offsetExists($keyRelations1 . '_' . $keyRelations2)) {
                            $this->currenciesRelationsIterator->offsetSet(
                                $keyRelations1 . '_' . $keyRelations2,
                                ++$numberRelations
                            );
                        }

                        if (!$this->currenciesRelationsIterator->offsetExists($keyRelations2 . '_' . $keyRelations1)) {
                            $this->currenciesRelationsIterator->offsetSet(
                                $keyRelations2 . '_' . $keyRelations1,
                                ++$numberRelations
                            );
                        }
                    }
                }
                if ($iMaxIdCurrency === $keyRelations1) {
                    $isBreak = true;
                }
            }
            if ($isBreak) {
                break;
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    private function getResponseRelationsCurrencies(): array
    {
        /** @var array $relationsListRequestByName */
        $relationsListRequestByName = [];

        foreach ($this->relationsListRequestByName as $relationsCurrency => $dummyNumber) {
            list($currency1, $currency2) = \explode('_', $relationsCurrency);

            /** @var int $idCurrency1 */
            $idCurrency1 = 0;
            /** @var int $idCurrency2 */
            $idCurrency2 = 0;

            foreach ($this->responseCurrenciesNumbered as $id => $data) {
                if ($currency1 == $data['name'] || $currency1 == $data['symbol']) {
                    $idCurrency1 = $id;
                    break;
                }
            }

            foreach ($this->responseCurrenciesNumbered as $id => $data) {
                if ($currency2 == $data['name'] || $currency2 == $data['symbol']) {
                    $idCurrency2 = $id;
                    break;
                }
            }

            $idRelations = $this->currenciesRelationsIterator->offsetGet($idCurrency1 . '_' . $idCurrency2);

            if (null !== $idRelations) {
                $relationsListRequestByName[$idRelations] = [
                    'id'   => $idRelations,
                    'sell' => $idCurrency1,
                    'buy'  => $idCurrency2,
                ];
            }
        }

        \ksort($relationsListRequestByName);

        return $relationsListRequestByName;
    }

    //endregion
}
