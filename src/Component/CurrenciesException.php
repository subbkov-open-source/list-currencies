<?php

declare(strict_types=1);

namespace SubbkovOpenSource\ListCurrencies\Component;

class CurrenciesException extends \Exception
{
}
