<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use SubbkovOpenSource\ListCurrencies\Component\Currencies;

class CurrenciesTest extends TestCase
{
    /** @var Currencies */
    protected $currenciesComponent;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->currenciesComponent = new Currencies();
    }

    /**
     * @return array
     */
    public function additionProviderMinMax(): array
    {
        return [
            'min' => [
                '{"EUR":[],"CAD":[],"RUB":[],"USD":[],"UAH":[],"BYN":[],"AUD":[],"GBP":[],"BRL":[],"HUF":[]}',
                '{"currencies":{"1":{"symbol":"EUR","name":"Euro","id":1},"2":{"symbol":"CAD","name":"Canadian Dollar","id":2},"3":{"symbol":"RUB","name":"Russian Ruble","id":3},"4":{"symbol":"USD","name":"US Dollar","id":4},"5":{"symbol":"UAH","name":"Hryvnia","id":5},"6":{"symbol":"BYN","name":"Belarussian Ruble","id":6},"27":{"symbol":"AUD","name":"Australian Dollar","id":27},"28":{"symbol":"GBP","name":"Pound Sterling","id":28},"29":{"symbol":"BRL","name":"Brazilian Real","id":29},"30":{"symbol":"HUF","name":"Forint","id":30}},"relations":{"1":{"id":1,"sell":1,"buy":2},"2":{"id":2,"sell":2,"buy":1},"3":{"id":3,"sell":1,"buy":3},"4":{"id":4,"sell":3,"buy":1},"5":{"id":5,"sell":1,"buy":4},"6":{"id":6,"sell":4,"buy":1},"7":{"id":7,"sell":1,"buy":5},"8":{"id":8,"sell":5,"buy":1},"9":{"id":9,"sell":1,"buy":6},"10":{"id":10,"sell":6,"buy":1},"51":{"id":51,"sell":2,"buy":3},"52":{"id":52,"sell":3,"buy":2},"53":{"id":53,"sell":2,"buy":4},"54":{"id":54,"sell":4,"buy":2},"55":{"id":55,"sell":2,"buy":5},"56":{"id":56,"sell":5,"buy":2},"57":{"id":57,"sell":2,"buy":6},"58":{"id":58,"sell":6,"buy":2},"99":{"id":99,"sell":3,"buy":4},"100":{"id":100,"sell":4,"buy":3},"101":{"id":101,"sell":3,"buy":5},"102":{"id":102,"sell":5,"buy":3},"103":{"id":103,"sell":3,"buy":6},"104":{"id":104,"sell":6,"buy":3},"145":{"id":145,"sell":4,"buy":5},"146":{"id":146,"sell":5,"buy":4},"147":{"id":147,"sell":4,"buy":6},"148":{"id":148,"sell":6,"buy":4},"189":{"id":189,"sell":5,"buy":6},"190":{"id":190,"sell":6,"buy":5},"651":{"id":651,"sell":1,"buy":27},"652":{"id":652,"sell":27,"buy":1},"653":{"id":653,"sell":1,"buy":28},"654":{"id":654,"sell":28,"buy":1},"655":{"id":655,"sell":1,"buy":29},"656":{"id":656,"sell":29,"buy":1},"657":{"id":657,"sell":1,"buy":30},"658":{"id":658,"sell":30,"buy":1},"703":{"id":703,"sell":2,"buy":27},"704":{"id":704,"sell":27,"buy":2},"705":{"id":705,"sell":2,"buy":28},"706":{"id":706,"sell":28,"buy":2},"707":{"id":707,"sell":2,"buy":29},"708":{"id":708,"sell":29,"buy":2},"709":{"id":709,"sell":2,"buy":30},"710":{"id":710,"sell":30,"buy":2},"755":{"id":755,"sell":3,"buy":27},"756":{"id":756,"sell":27,"buy":3},"757":{"id":757,"sell":3,"buy":28},"758":{"id":758,"sell":28,"buy":3},"759":{"id":759,"sell":3,"buy":29},"760":{"id":760,"sell":29,"buy":3},"761":{"id":761,"sell":3,"buy":30},"762":{"id":762,"sell":30,"buy":3},"807":{"id":807,"sell":4,"buy":27},"808":{"id":808,"sell":27,"buy":4},"809":{"id":809,"sell":4,"buy":28},"810":{"id":810,"sell":28,"buy":4},"811":{"id":811,"sell":4,"buy":29},"812":{"id":812,"sell":29,"buy":4},"813":{"id":813,"sell":4,"buy":30},"814":{"id":814,"sell":30,"buy":4},"859":{"id":859,"sell":5,"buy":27},"860":{"id":860,"sell":27,"buy":5},"861":{"id":861,"sell":5,"buy":28},"862":{"id":862,"sell":28,"buy":5},"863":{"id":863,"sell":5,"buy":29},"864":{"id":864,"sell":29,"buy":5},"865":{"id":865,"sell":5,"buy":30},"866":{"id":866,"sell":30,"buy":5},"911":{"id":911,"sell":6,"buy":27},"912":{"id":912,"sell":27,"buy":6},"913":{"id":913,"sell":6,"buy":28},"914":{"id":914,"sell":28,"buy":6},"915":{"id":915,"sell":6,"buy":29},"916":{"id":916,"sell":29,"buy":6},"917":{"id":917,"sell":6,"buy":30},"918":{"id":918,"sell":30,"buy":6},"2003":{"id":2003,"sell":27,"buy":28},"2004":{"id":2004,"sell":28,"buy":27},"2005":{"id":2005,"sell":27,"buy":29},"2006":{"id":2006,"sell":29,"buy":27},"2007":{"id":2007,"sell":27,"buy":30},"2008":{"id":2008,"sell":30,"buy":27},"2053":{"id":2053,"sell":28,"buy":29},"2054":{"id":2054,"sell":29,"buy":28},"2055":{"id":2055,"sell":28,"buy":30},"2056":{"id":2056,"sell":30,"buy":28},"2101":{"id":2101,"sell":29,"buy":30},"2102":{"id":2102,"sell":30,"buy":29}}}',
            ],
            'max' => [
                '{"BTC":[],"USD":["BTC","EUR"],"BCH":["BNB","SDT"]}',
                '{"currencies":{"1":{"symbol":"EUR","name":"Euro","id":1},"4":{"symbol":"USD","name":"US Dollar","id":4},"7":{"symbol":"BTC","name":"Bitcoin","id":7},"10":{"symbol":"BCH","name":"Bitcoin Cash","id":10},"22":{"symbol":"BNB","name":"Binance Coin","id":22},"1914":{"symbol":"SDT","name":"Alchemint","id":1914}},"relations":{"5":{"id":5,"sell":1,"buy":4},"6":{"id":6,"sell":4,"buy":1},"149":{"id":149,"sell":4,"buy":7},"150":{"id":150,"sell":7,"buy":4},"155":{"id":155,"sell":4,"buy":10},"156":{"id":156,"sell":10,"buy":4},"275":{"id":275,"sell":7,"buy":10},"276":{"id":276,"sell":10,"buy":7},"401":{"id":401,"sell":10,"buy":22},"402":{"id":402,"sell":22,"buy":10},"516341":{"id":516341,"sell":10,"buy":1914},"516342":{"id":516342,"sell":1914,"buy":10}}}',
            ],
        ];
    }

    /**
     * @dataProvider additionProviderMinMax
     *
     * @param string $testData
     * @param string $resultData
     *
     * @throws Exception
     */
    public function testGetDataMinMax(string $testData, string $resultData): void
    {
        $testArray       = \json_decode($testData, true);
        $resultArray     = \json_decode($resultData, true);
        $resultComponent = $this->currenciesComponent->getData($testArray);

        self::assertSame($resultArray, $resultComponent);
    }
}
