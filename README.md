# list-currencies

Компонент для работы со списками валют и криптовалют.

**Назначение**: создает ID валют и ID связей валют между собой.

**Необходимость**: использовать одинаковые ID в разных проектах при пересечении проектов между собой.

**Как работает**:
1. создать объект класса `Currencies()`
2. получить массив валют и связей с помощью вызова метода `->getData($yaml)`, передав в него массив необходимых валют.


В ответ получите массив содержащий 2 элемента
1. `currencies` - массив значений валют
2. `relations` - массив связей валют


Структура передаваемого массива:
````
[BTC] => [],
[USD] => 
	[
	    [] => BTC,
	    [] => EUR
	],
[BCH] => 
	[
	    [] => BNB
	]
````

Структуру yaml файла для преобразования в массив:
````
BTC: {  }
USD:
    - BTC
    - EUR
BCH:
    - BNB
````
   	
Если не указывать для выбранной валюты связи `(BTC: {  })`, то связи будут созданы для общего списка валют: BTC, USD, USD. Тем самы будут созданы связи:
````
BTC_USD
USD_BTC
BTC_BCH
BCH_BTC
USD_BCH
BCH_USD
````

Если указать для выбранной валюты связи `(USD: {BTC, EUR})`, то дополнительно к общему списку (см выше) будут созданы связи:
````
USD_BTC
BTC_USD
USD_EUR
EUR_USD
````

Указывать можно как символы `USD` валют, так и их названия `US Dollar`.

Список используемых валют содержится в файле `src/Data/list-currencies.yaml`